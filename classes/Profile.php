<?php

/**
 * Class to handle articles
 */

class Profile
{

  // Properties

  public $user_id = null;
  public $First_Name = null;
  public $Last_Name = null;
  public $Birthday = null;
  public $Occupation = null;
  public $Email = null;
  public $Company = null;
  public $Phone_Number = null;
  public $Picture = null;
  public $Blurb = null;
  public $seeking_mentor = null;
  public $seeking_mentee = null;
  public $mentor = null;
  public $mentee = null;
  public $Location = null;
  public $life_stage = null;
  public $field = null;
  public $language = null;


  /**
  * Sets the object's properties using the values in the supplied array
  *
  * @param assoc The property values
  */

  public function __construct( $data=array() ) {
    if ( isset( $data['First_Name'] ) ) $this->First_Name =  $data['First_Name'];
    if ( isset( $data['user_id'] ) ) $this->user_id =  $data['user_id'];
    if ( isset( $data['life_stage'] ) ) $this->life_stage =  $data['life_stage'];
    if ( isset( $data['field'] ) ) $this->field =  $data['field'];
    if ( isset( $data['language'] ) ) $this->language =  $data['language'];
    if ( isset( $data['month'] ) && isset( $data['day'] ) && isset( $data['year'] )) {$this->Birthday =  $data['year']."-".$data['month']."-".$data['day'];} elseif( isset( $data['Birthday'] )){ $this->Birthday =  $data['Birthday'];}
    if ( isset( $data['Occupation'] ) ) $this->Occupation =  $data['Occupation'];
    if ( isset( $data['Last_Name'] ) ) $this->Last_Name =  $data['Last_Name'];
    if ( isset( $data['Email'] ) ) $this->Email =  $data['Email'];
    if ( isset( $data['Company'] ) ) $this->Company =  $data['Company'];
    if ( isset( $data['Phone_Number'] ) ) $this->Phone_Number =  $data['Phone_Number'];
    if ( isset( $data['Picture'] ) ) $this->Picture =  $data['Picture'];
    if ( isset( $data['Blurb'] ) ) $this->Blurb =  $data['Blurb'];
    if ( isset( $data['seeking_mentor'] ) ) $this->seeking_mentor = (int) $data['seeking_mentor'];
    if ( isset( $data['seeking_mentee'] ) ) $this->seeking_mentee  = (int) $data['seeking_mentee'];
    if ( isset( $data['mentor'] ) ) $this->mentor =  $data['mentor'];
    if ( isset( $data['mentee'] ) ) $this->mentee =  $data['mentee'];
    if ( isset( $data['Location'] ) ) $this->Location = $data['Location'];

  }

public function storeFormValues ( $params ) {

    // Store all the parameters
    $this->__construct( $params );

    // Parse and store the publication date

      }


  /**
  * Returns an Profile object matching the given username
  *

  */

  public static function getByUser( $user_id ) {
    $conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
    $sql = "SELECT * FROM profile WHERE user_id = :user_id";
    $st = $conn->prepare( $sql );
    $st->bindValue( ":user_id", $user_id, PDO::PARAM_STR );
    $st->execute();
    $row = $st->fetch();
    $conn = null;
    if ( $row ) return new Profile( $row );
  }
  
 public static function getUser( $user_id ) {
    $conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
    $sql = "SELECT * FROM profile WHERE user_id = :user_id";
    $st = $conn->prepare( $sql );
    $st->bindValue( ":user_id", $user_id, PDO::PARAM_STR );
    $st->execute();
    $row = $st->fetch();
    $conn = null;
        if ( $row ) {
    return 1;
    } else {return 0 ;};
  }


  public static function getList( $numRows=1000000 ) {
    $conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
    $sql = "SELECT user_id, First_Name, Last_Name FROM profile LIMIT 10";
    $st = $conn->prepare( $sql );
    $st->bindValue( ":numRows", $numRows, PDO::PARAM_INT );
    $st->execute();
    $list = array();

    while ( $row = $st->fetch() ) {
      $profile = new Profile( $row );
      $list[] = $profile;
    }

    // Now get the total number of articles that matched the criteria
    $sql = "SELECT FOUND_ROWS() AS totalRows";
    $totalRows = $conn->query( $sql )->fetch();
    $conn = null;
    return ( array ( "results" => $list, "totalRows" => $totalRows[0] ) );
  }


  public static function getListMentors( $user_id ) {
    $conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
    $sql = "SELECT user_id, First_Name, Last_Name, Email FROM profile WHERE user_id in (SELECT bio_user_id FROM mentors WHERE bio_user_id1 = :user_id) LIMIT 10";
    $st = $conn->prepare( $sql );
    $st->bindValue( ":user_id", $user_id, PDO::PARAM_STR );
    $st->execute();
    $list = array();

    while ( $row = $st->fetch() ) {
      $profile = new Profile( $row );
      $list[] = $profile;
    }

    // Now get the total number of articles that matched the criteria
    $sql = "SELECT FOUND_ROWS() AS totalRows";
    $totalRows = $conn->query( $sql )->fetch();
    $conn = null;
    return ( array ( "results" => $list, "totalRows" => $totalRows[0] ) );
  }


  public static function getListMentees( $user_id ) {
    $conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
    $sql = "SELECT user_id, First_Name, Last_Name, Email FROM profile WHERE user_id in (SELECT bio_user_id1 FROM mentors WHERE bio_user_id = :user_id) LIMIT 10";
    $st = $conn->prepare( $sql );
    $st->bindValue( ":user_id", $user_id, PDO::PARAM_STR );
    $st->execute();
    $list = array();

    while ( $row = $st->fetch() ) {
      $profile = new Profile( $row );
      $list[] = $profile;
    }

    // Now get the total number of articles that matched the criteria
    $sql = "SELECT FOUND_ROWS() AS totalRows";
    $totalRows = $conn->query( $sql )->fetch();
    $conn = null;
    return ( array ( "results" => $list, "totalRows" => $totalRows[0] ) );
  }


  /**
  * Inserts the current Article object into the database, and sets its ID property.
  */

  public function insert() {

    // Does the Article object already have an ID?

    // Insert the Article
    $conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
    $sql = "INSERT INTO profile( user_id, First_Name, Last_Name, Birthday, Occupation, life_stage, field, language, Email, Company, Phone_Number, Picture, Blurb, seeking_mentor, seeking_mentee, mentor, mentee, Location ) VALUES ( :user_id, :First_Name, :Last_Name, :Birthday, :Occupation,:life_stage, :field, :language, :Email, :Company, :Phone_Number, :Picture, :Blurb, :seeking_mentor, :seeking_mentee, :mentor, :mentee, :Location )";
    $st = $conn->prepare ( $sql );
    $st->bindValue( ":user_id", $this->user_id, PDO::PARAM_STR );
    $st->bindValue( ":First_Name", $this->First_Name, PDO::PARAM_STR );
    $st->bindValue( ":Last_Name", $this->Last_Name, PDO::PARAM_STR );
    $st->bindValue( ":Birthday", $this->Birthday, PDO::PARAM_STR );
    $st->bindValue( ":Occupation", $this->Occupation, PDO::PARAM_STR );
     $st->bindValue( ":life_stage", $this->life_stage, PDO::PARAM_STR );
    $st->bindValue( ":field", $this->field, PDO::PARAM_STR );
    $st->bindValue( ":language", $this->language, PDO::PARAM_STR );   
    $st->bindValue( ":Email", $this->Email, PDO::PARAM_STR );
    $st->bindValue( ":Company", $this->Company, PDO::PARAM_STR );
    $st->bindValue( ":Phone_Number", $this->Phone_Number, PDO::PARAM_STR );
    $st->bindValue( ":Picture", $this->Picture, PDO::PARAM_STR );
    $st->bindValue( ":Blurb", $this->Blurb, PDO::PARAM_STR );
    $st->bindValue( ":seeking_mentor", $this->seeking_mentor, PDO::PARAM_STR );
    $st->bindValue( ":seeking_mentee", $this->seeking_mentee, PDO::PARAM_STR );
    $st->bindValue( ":mentor", $this->mentor, PDO::PARAM_INT );
    $st->bindValue( ":mentee", $this->mentee, PDO::PARAM_INT );
    $st->bindValue( ":Location", $this->Location, PDO::PARAM_STR );
    $st->execute();
   // $this->user_id = $conn->lastInsertId();
    $conn = null;
  }



  /**
  * Updates the current Article object in the database.
  */

  public function update() {

    // Does the Article object have an ID?
    if ( is_null( $this->user_id ) ) trigger_error ( "Profile::update(): Attempt to update an Profile object that does not have its ID property set.", E_USER_ERROR );

    // Update the Article
    $conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
    $sql = "REPLACE INTO profile( user_id, First_Name, Last_Name, Birthday, Occupation, life_stage, field, language, Email, Company, Phone_Number, Picture, Blurb, seeking_mentor, seeking_mentee, mentor, mentee, Location ) VALUES ( :user_id, :First_Name, :Last_Name, :Birthday, :Occupation,:life_stage, :field, :language, :Email, :Company, :Phone_Number, :Picture, :Blurb, :seeking_mentor, :seeking_mentee, :mentor, :mentee, :Location )";
    $st = $conn->prepare ( $sql );
    $st->bindValue( ":user_id", $this->user_id, PDO::PARAM_STR );
    $st->bindValue( ":First_Name", $this->First_Name, PDO::PARAM_STR );
    $st->bindValue( ":Last_Name", $this->Last_Name, PDO::PARAM_STR );
    $st->bindValue( ":Birthday", $this->Birthday, PDO::PARAM_STR );
    $st->bindValue( ":Occupation", $this->Occupation, PDO::PARAM_STR );
     $st->bindValue( ":life_stage", $this->life_stage, PDO::PARAM_STR );
    $st->bindValue( ":field", $this->field, PDO::PARAM_STR );
    $st->bindValue( ":language", $this->language, PDO::PARAM_STR );   
    $st->bindValue( ":Email", $this->Email, PDO::PARAM_STR );
    $st->bindValue( ":Company", $this->Company, PDO::PARAM_STR );
    $st->bindValue( ":Phone_Number", $this->Phone_Number, PDO::PARAM_STR );
    $st->bindValue( ":Picture", $this->Picture, PDO::PARAM_STR );
    $st->bindValue( ":Blurb", $this->Blurb, PDO::PARAM_STR );
    $st->bindValue( ":seeking_mentor", $this->seeking_mentor, PDO::PARAM_STR );
    $st->bindValue( ":seeking_mentee", $this->seeking_mentee, PDO::PARAM_STR );
    $st->bindValue( ":mentor", $this->mentor, PDO::PARAM_INT );
    $st->bindValue( ":mentee", $this->mentee, PDO::PARAM_INT );
    $st->bindValue( ":Location", $this->Location, PDO::PARAM_STR );
    $st->execute();
    //$this->id = $conn->lastInsertId();
    $conn = null;
  }


  /**
  * Deletes the current Article object from the database.
  */

  public function delete() {

    // Does the Article object have an ID?
    if ( is_null( $this->user_id ) ) trigger_error ( "Profile::delete(): Attempt to delete an Profile object that does not have its ID property set.", E_USER_ERROR );

    // Delete the Article
    $conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
    $st = $conn->prepare ( "DELETE FROM articles WHERE user_id = :user_id LIMIT 1" );
    $st->bindValue( ":user_id", $this->user_id, PDO::PARAM_STR );
    $st->execute();
    $conn = null;
  }

}

?>
