<?php

/**
 * Class to handle articles
 */

class User
{

  // Properties

  public $id = null;
  public $password = null;


  /**
  * Sets the object's properties using the values in the supplied array
  *
  * @param assoc The property values
  */

  public function __construct( $data=array() ) {
    if ( isset( $data['id'] ) ) $this->id =  $data['id'];
    if ( isset( $data['password'] ) ) $this->password =  $data['password'];

  }
  
public function storeFormValues ( $params ) {

    // Store all the parameters
    print_r($params);
    $this->__construct( $params );

    // Parse and store the publication date

      }


  /**
  * Returns an Profile object matching the given username
  *

  */

  public static function getByUser( $var1, $var2 ) {
    $conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
    $sql = "SELECT * FROM user WHERE id = :id and password = :password";
    $st = $conn->prepare( $sql );
    $st->bindValue( ":id", $var1, PDO::PARAM_STR );
    $st->bindValue( ":password", $var2, PDO::PARAM_STR );
    $st->execute();
    $row = $st->fetch();
    $conn = null;
    if ( $row ) {
    return 1;
    } else {return 0 ;};
  }



  /**
  * Inserts the current Article object into the database, and sets its ID property.
  */

  public function insert() {

    // Does the Article object already have an ID?
    // Insert the Article
    $conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
    $sql = "INSERT INTO user(id, password) VALUES(:id, :password)";
    $st = $conn->prepare ( $sql );
    $st->bindValue( ":id", $this->id, PDO::PARAM_STR );
    $st->bindValue( ":password", $this->password, PDO::PARAM_STR );
    $st->execute();
    $this->user_id = $conn->lastInsertId();
    $conn = null;
  }



  /**
  * Updates the current Article object in the database.
  */

  public function update() {

    // Does the Article object have an ID?
    if ( is_null( $this->user_id ) ) trigger_error ( "Profile::update(): Attempt to update an Profile object that does not have its ID property set.", E_USER_ERROR );

    // Update the Article
    $conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
    $sql = "REPLACE INTO user(id, password) VALUES(:id, :password)";
    $st = $conn->prepare ( $sql );
    $st->bindValue( ":id", $this->id, PDO::PARAM_STR );
    $st->bindValue( ":password", $this->password, PDO::PARAM_STR );
    $st->execute();
    $this->id = $conn->lastInsertId();
    $conn = null;
  }


  /**
  * Deletes the current Article object from the database.
  */

  public function delete() {

    // Does the Article object have an ID?
    if ( is_null( $this->user_id ) ) trigger_error ( "Profile::delete(): Attempt to delete an Profile object that does not have its ID property set.", E_USER_ERROR );

    // Delete the Article
    $conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
    $st = $conn->prepare ( "DELETE FROM articles WHERE user_id = :user_id LIMIT 1" );
    $st->bindValue( ":user_id", $this->user_id, PDO::PARAM_INT );
    $st->execute();
    $conn = null;
  }

}

?>
