<?php

/**
 * Class to handle articles
 */

class Bio
{

  // Properties

  public $user_id = null;
  public $life_stage = null;
  public $location = null;
  public $field = null;
  public $language = null;
  public $mentor = null;


  /**
  * Sets the object's properties using the values in the supplied array
  *
  * @param assoc The property values
  */

  public function __construct( $data=array() ) {
    if ( isset( $data['user_id'] ) ) $this->user_id =  $data['user_id'];
    if ( isset( $data['life_stage'] ) ) $this->life_stage =  $data['life_stage'];
    if ( isset( $data['location'] ) ) $this->user_id =  $data['location'];
    if ( isset( $data['field'] ) ) $this->user_id =  $data['field'];
    if ( isset( $data['language'] ) ) $this->user_id =  $data['language'];
    if ( isset( $data['mentor'] ) ) $this->user_id =  $data['mentor'];
  }

public function storeFormValues ( $params ) {

    // Store all the parameters
    print_r($params);
    $this->__construct( $params );

    // Parse and store the publication date

      }


  /**
  * Returns an Profile object matching the given username
  *

  */

  public static function getByUser( $user_id ) {
    $conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
    $sql = "SELECT * FROM seeking WHERE user_id = :user_id";
    $st = $conn->prepare( $sql );
    $st->bindValue( ":user_id", $user_id, PDO::PARAM_STR );
    $st->execute();
    $row = $st->fetch();
    $conn = null;
    if ( $row ) return new Profile( $row );
  }
  
 public static function getUser( $user_id ) {
    $conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
    $sql = "SELECT * FROM seeking WHERE user_id = :user_id";
    $st = $conn->prepare( $sql );
    $st->bindValue( ":user_id", $user_id, PDO::PARAM_STR );
    $st->execute();
    $row = $st->fetch();
    $conn = null;
        if ( $row ) {
    return 1;
    } else {return 0 ;};
  }


  public function insert() {

    // Does the Article object already have an ID?

    // Insert the Article
    $conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
    $sql = "INSERT INTO seeking( user_id, life_stage, location, field, language, mentor ) VALUES(:user_id, :life_stage, :location, :field, :language, :mentor)";
    $st = $conn->prepare ( $sql );
    $st->bindValue( ":user_id", $this->user_id, PDO::PARAM_STR );
    $st->bindValue( ":life_stage", $this->life_stage, PDO::PARAM_STR );
    $st->bindValue( ":location", $this->location, PDO::PARAM_STR );
    $st->bindValue( ":field", $this->field, PDO::PARAM_STR );
    $st->bindValue( ":language", $this->language, PDO::PARAM_STR );
    $st->bindValue( ":mentor", $this->mentor, PDO::PARAM_STR );
    $st->execute();
   // $this->user_id = $conn->lastInsertId();
    $conn = null;
  }



  /**
  * Updates the current Article object in the database.
  */

  public function update() {

    // Insert the Article
    $conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
    $sql = "REPLACE INTO seeking( user_id, life_stage, location, field, language, mentor ) VALUES(:user_id, :life_stage, :location, :field, :language, :mentor)";
    $st = $conn->prepare ( $sql );
    $st->bindValue( ":user_id", $this->user_id, PDO::PARAM_STR );
    $st->bindValue( ":life_stage", $this->life_stage, PDO::PARAM_STR );
    $st->bindValue( ":location", $this->location, PDO::PARAM_STR );
    $st->bindValue( ":field", $this->field, PDO::PARAM_STR );
    $st->bindValue( ":language", $this->language, PDO::PARAM_STR );
    $st->bindValue( ":mentor", $this->mentor, PDO::PARAM_STR );
    $st->execute();
    //$this->id = $conn->lastInsertId();
    $conn = null;
  }


  /**
  * Deletes the current Article object from the database.
  */

  public function delete() {

    // Delete the Article
    $conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
    $st = $conn->prepare ( "DELETE FROM seeking WHERE user_id = :user_id LIMIT 1" );
    $st->bindValue( ":user_id", $this->user_id, PDO::PARAM_STR );
    $st->execute();
    $conn = null;
  }

}

?>
