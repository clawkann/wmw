<?php include "templates/include/header.php" ?>

      <h1><?php echo $results['pageTitle']?></h1>

      <form action="index.php?action=<?php echo $results['formAction']?>" method="post">

<?php if ( isset( $results['errorMessage'] ) ) { ?>
        <div class="errorMessage"><?php echo $results['errorMessage'] ?></div>
<?php } ?>

   


 <div class = vert2>
     <div>
        <label for="First_Name">First_Name</label>
        <textarea name="First_Name" id="First_Name" placeholder="First Name" required maxlength="50" ;"><?php echo htmlspecialchars( $results['profile']->First_Name )?></textarea>
    </div>
    <div>
        <label for="Last_Name">Last_Name</label>
        <textarea name="Last_Name" id="Last_Name" placeholder="Last Name" required maxlength="50" ;"><?php echo htmlspecialchars( $results['profile']->Last_Name )?></textarea>
    </div>
</div>
<div>
    <label for="Blurb">Blurb</label>
    <textarea name="Blurb" id="Blurb" placeholder="Tell us about yourself"  maxlength="1000" style="height: 5em; width:80%;"><?php echo htmlspecialchars( $results['profile']->Blurb )?></textarea>
 </div>    
    <div class = vert3>
            <label for="Birthday">Birthday:</label>
            <?php if($results['profile']->Birthday){list($year, $month, $day) = explode("-", $results['profile']->Birthday,3 );} else{$month = NULL; $day = NULL; $year = NULL;} ?> 
<div>
            <select name="month">
              <option value = <?php echo $month;?> selected><?php if ($month){
                                                                    if ($month == "01"){
                                                                        echo "Jan";}
                                                                    elseif($month == "02" ){
                                                                        echo "Feb";}
                                                                    elseif($month == "03" ){
                                                                        echo "Mar";}
                                                                        elseif($month == "04" ){
                                                                        echo "Apr";}
                                                                    elseif($month == "05" ){
                                                                        echo "May";}
                                                                        elseif($month == "06" ){
                                                                        echo "Jun";}
                                                                    elseif($month == "07" ){
                                                                        echo "Jul";}
                                                                        elseif($month == "08" ){
                                                                        echo "Aug";}
                                                                    elseif($month == "09" ){
                                                                        echo "Sept";}
                                                                        elseif($month == "10" ){
                                                                        echo "Oct";}
                                                                    elseif($month == "11" ){
                                                                        echo "Nov";}
                                                                        elseif($month == "12" ){
                                                                        echo "Dec";}}
                                                                 else{
                                                                    echo "Month";}?></option>
              <option value="01">Jan</option>
              <option value="02" >Feb</option>
              <option value = "03">Mar</option>
              <option value="04">Apr</option>
              <option value="05" >May</option>
              <option value = "06">Jun</option>
              <option value="07">Jul</option>
              <option value="08" >Aug</option>
              <option value = "09">Sept</option>
              <option value="10">Oct</option>
              <option value="11" >Nov</option>
              <option value = "12">Dec</option>
            </select>
            </div>
            <div>
            <select name="day">
              <option value = <?php echo $day;?> selected><?php if($day){echo $day;} else {echo "Day";}?></option>
              <option value="01">1</option>
              <option value="02" >2</option>
              <option value = "03">3</option>
              <option value="04">4</option>
              <option value="05" >5</option>
              <option value = "06">6</option>
              <option value="07">7</option>
              <option value="08" >8</option>
              <option value = "09">9</option>
              <option value="10">10</option>
              <option value="11" >11</option>
              <option value = "12">12</option>
              <option value="13">13</option>
              <option value="14" >14</option>
              <option value = "15">15</option>
              <option value="16">16</option>
              <option value="17" >17</option>
              <option value = "18">18</option>
              <option value="19">19</option>
              <option value="20" >20</option>
              <option value = "21">21</option>
              <option value="22">22</option>
              <option value="23" >23</option>
              <option value = "24">24</option>
              <option value="25">25</option>
              <option value="26" >26</option>
              <option value = "27">27</option>
              <option value="28">28</option>
              <option value="29" >29</option>
              <option value = "30">30</option>
              <option value="31">31</option>            
            </select>
                </div>
            <div>
            <select name="year">
              <option value = <?php echo $year;?> selected><?php if($year){echo $year;} else {echo "Year";}?></option>
              <option value="2010">2010</option>
              <option value="2009" >2009</option>
              <option value="2008">2008</option>
              <option value="2007" >2007</option>
              <option value="2006">2006</option>
              <option value="2005" >2005</option>
              <option value="2004">2004</option>
              <option value="2003" >2003</option>
              <option value="2002">2002</option>
              <option value="2001" >2001</option>
              <option value="2000">2000</option>  
              <option value="1999">1999</option>
              <option value="1998" >1998</option> 
              <option value="1997">1997</option>
              <option value="1996" >1996</option> 
              <option value="1995">1995</option>
              <option value="1994" >1994</option> 
              <option value="1993">1993</option>
              <option value="1992" >1992</option> 
              <option value="1991">1991</option>
              <option value="1990" >1990</option> 
              <option value="1989">1989</option>
              <option value="1988" >1988</option> 
              <option value="1987">1987</option>
              <option value="1986" >1986</option> 
              <option value="1985">1985</option>
              <option value="1984" >1984</option> 
              <option value="1983">1983</option>
              <option value="1982" >1982</option> 
              <option value="1981">1981</option>
              <option value="1980" >1980</option>
              <option value="1979">1979</option>
              <option value="1978" >1978</option> 
              <option value="1977">1977</option>
              <option value="1976" >1976</option> 
              <option value="1975">1975</option>
              <option value="1974" >1974</option> 
              <option value="1973">1973</option>
              <option value="1972" >1972</option> 
              <option value="1971">1971</option>
              <option value="1970" >1970</option>  
              <option value="1969">1989</option>
              <option value="1968" >1988</option> 
              <option value="1967">1987</option>
              <option value="1966" >1986</option> 
              <option value="1965">1985</option>
              <option value="1964" >1984</option> 
              <option value="1963">1983</option>
              <option value="1962" >1982</option> 
              <option value="1961">1981</option>
              <option value="1960" >1980</option>
              <option value="1959">1979</option>
              <option value="1958" >1978</option> 
              <option value="1957">1977</option>
              <option value="1956" >1976</option> 
              <option value="1955">1975</option>
              <option value="1954" >1974</option> 
              <option value="1953">1973</option>
              <option value="1952" >1972</option> 
              <option value="1951">1971</option>
              <option value="1950" >1970</option>                                                                  
            </select>
    </div>
</div>

<div class = vert2>
    <div>
            <label for="life_stage">Life Stage:</label>
            <select name="life_stage">
              <option value = <?php echo htmlspecialchars( $results['profile']->life_stage )?> selected><?php if ($results['profile']->life_stage) {
                                                                                                                echo $results['profile']->life_stage;}
                                                                                                            else{
                                                                                                                echo "Life Stage";}?></option>
              <option value="Middle_School">Middle_School</option>
              <option value="High_School" >High_School</option>
              <option value = "Undergrad">Undergrad</option>
              <option value="Graduate_School" >Graduate_School</option>
              <option value = "Early_Professional">Early_Professional</option>
              <option value = "Mid_Professional">Mid_Professional</option>
              <option value = "Late_Professional">Late_Professional</option>
            </select>
    </div>  
    <div> 
        <label for="field">Professional Field</label>
        <textarea name="field" id="field" placeholder="Professional Field"  maxlength="100" ;"><?php echo htmlspecialchars( $results['profile']->field )?></textarea>   
    </div>
    </div>
<div class = vert2>
    <div>
        <label for="Occupation">Occupation</label>
        <textarea name="Occupation" id="Occupation" placeholder="Occupation"  maxlength="100" ;"><?php echo htmlspecialchars( $results['profile']->Occupation )?></textarea>
    </div>  
    <div>
        <label for="Company">Company</label>
        <textarea name="Company" id="Company" placeholder="Company"  maxlength="50" ;"><?php echo htmlspecialchars( $results['profile']->Company )?></textarea>
    </div>
</div>  

<div class = vert2>
    <div>    
        <label for="Email">Email</label>
        <textarea name="Email" id="Email" placeholder="Email"  maxlength="100" ;"><?php echo htmlspecialchars( $results['profile']->Email )?></textarea>
   </div>
   <div> 
       <label for="Phone_Number">Phone_Number</label>
       <textarea name="Phone_Number" id="Phone_Number" placeholder="xxx-xxx-xxxx"  maxlength="1000" ;"><?php echo htmlspecialchars( $results['profile']->Phone_Number )?></textarea>
   </div>
</div>

<div class = vert2>
    <div>
        <label for="Location">Location </label>
        <textarea name="Location " id="Location " placeholder="Location "  maxlength="100" ;"><?php echo htmlspecialchars( $results['profile']->Location  )?></textarea>
    </div>
    <div>     
        <label for="language">language:</label>
        <select name="language">
          <option value = <?php echo htmlspecialchars( $results['profile']->language )?> selected><?php if ($results['profile']->language) {
                                                                                                        if ($results['profile']->language == "en" ){
                                                                                                            echo "English";}
                                                                                                        elseif($results['profile']->language == "fr" ){
                                                                                                            echo "French";}
                                                                                                        elseif($results['profile']->language == "sp" ){
                                                                                                            echo "Spanish";}}
                                                                                                    else{
                                                                                                        echo "Preferred Language";}?></option>
          <option value="en">English</option>
          <option value="fr" >French</option>
          <option value = "sp">Spanish</option>
        </select>
    </div>
</div>
 
       

            <label for="Picture">Picture</label>
            <textarea name="Picture" id="Picture" placeholder="Picture"  maxlength="1000" style="height: 5em;"><?php echo htmlspecialchars( $results['profile']->Picture )?></textarea>
        


<div class = vert3close>
    <div>
            <lp> </p>
    </div>
    <div>              
            <p> Yes </p>  
    </div>
    <div>              
            <p> No </p>  
    </div>
</div> 
<div class = vert3close>
    <div>
        <p> Seeking Mentor </p>
    </div>
    <div>  
        <input type="radio" <?php if( $results['profile']-> seeking_mentor){echo 'checked="checked"';} ?> name="seeking_mentor" value = 1>
    </div>
    <div> 
        <input type="radio" <?php if( !$results['profile']-> seeking_mentor){echo 'checked="checked"';} ?> name="seeking_mentor" value = 0>
    </div>
</div> 
<div class = vert3close>
    <div>
        <p> Seeking Mentee </p>
    </div>
    <div>  
        <input type="radio" <?php if( $results['profile']-> seeking_mentee){echo 'checked="checked"';} ?>  name="seeking_mentee" value = 1>
    </div>
    <div> 
        <input type="radio" <?php if( !$results['profile']-> seeking_mentee){echo 'checked="checked"';} ?> name="seeking_mentee" value = 0>
    </div>
</div> 
        
     
        <div class="buttons">
          <input type="submit" name="saveChanges" value="Save Changes" />
          <input type="submit" formnovalidate name="cancel" value="Cancel" />
        </div>

      </form>

<?php include "templates/include/footer.php" ?>
