<?php include "templates/include/header.php" ?>


      <form action="mentorship.php?action=<?php echo $results['formAction']?>" method="post" style="width:100%;">
<img src="lp.jpg" alt="banner" class="banner">
<?php if ( isset( $results['errorMessage'] ) ) { ?>
        <div class="errorMessage"><?php echo $results['errorMessage'] ?></div>
<?php } ?>

 <div class = vert2>
    <div>
            <label for="life_stage">Life Stage:</label>
            <select name="life_stage">
              <option value = <?php echo htmlspecialchars( $results['request']->life_stage )?> selected><?php if ($results['request']->life_stage) {
                                                                                                                echo $results['request']->life_stage;}
                                                                                                            else{
                                                                                                                echo "Life Stage";}?></option>
              <option value="Middle_School">Middle_School</option>
              <option value="High_School" >High_School</option>
              <option value = "Undergrad">Undergrad</option>
              <option value="Graduate_School" >Graduate_School</option>
              <option value = "Early_Professional">Early_Professional</option>
              <option value = "Mid_Professional">Mid_Professional</option>
              <option value = "Late_Professional">Late_Professional</option>
            </select>
    </div>  
    <div> 
        <label for="field">Professional Field</label>
        <textarea name="field" id="field" placeholder="Professional Field"  maxlength="100" ;"><?php echo htmlspecialchars( $results['request']->field )?></textarea>   
    </div>
</div>


<div class = vert2>
    <div>     
        <label for="location">location</label>
        <textarea name="location" id="location" placeholder="location" maxlength="100"style="height: 2.5em;"><?php echo htmlspecialchars( $results['request']->location )?></textarea>
    </div>
    <div>     
        <label for="language">language:</label>
        <select name="language">
          <option value = <?php echo htmlspecialchars( $results['request']->language )?> selected><?php if ($results['request']->language) {
                                                                                                        if ($results['request']->language == "en" ){
                                                                                                            echo "English";}
                                                                                                        elseif($results['request']->language == "fr" ){
                                                                                                            echo "French";}
                                                                                                        elseif($results['request']->language == "sp" ){
                                                                                                            echo "Spanish";}}
                                                                                                    else{
                                                                                                        echo "Preferred Language";}?></option>
          <option value="en">English</option>
          <option value="fr" >French</option>
          <option value = "sp">Spanish</option>
        </select>
    </div>
</div>


<?php if ($ment == 2) { ?>
<div class = vert3close>
    <div>
       <p> </p>
    </div>
    <div>
        <p> Mentor </p>    
    </div>
    <div>
        <p> Mentee</p>
    </div>
</div>
<div class = vert3close>
    <div>
        <p> Seeking </p>
    </div>
    <div>
        <input type="radio"  name="mentor" value = 1>
    </div>
    <div>
        <input type="radio" name="mentor" value = 0>
    </div>
</div>

<?php } elseif ($ment = 1){ ?>
    <div>
<input type="radio" checked="checked" name="mentor" value = 1>
    </div>
<?php } else { ?>
    <div>
<input type="radio" checked="checked" name="mentor" value = 0>
    </div>
<?php }?>






        <div class="buttons">
          <input type="submit" name="saveChanges" value="Save Changes" />
          <input type="submit" formnovalidate name="cancel" value="Cancel" />
        </div>

      </form>

<?php if ( $results['request']->user_id ) { ?>
      <p><a href="mentorship.php?action=deleteRequest&amp;userId=<?php echo $results['request']->user_id ?>&amp;mentor=<?php echo $results['request']->mentor?>" onclick="return confirm('Delete This Request?')">Delete This Request</a></p>
<?php } ?>

<?php include "templates/include/footer.php" ?>
