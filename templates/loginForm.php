<?php include "templates/include/headerLogin.php" ?>

      <form action="index.php?action=login" method="post" style="width: 40%;">
        <input type="hidden" name="login" value="true" />

<?php if ( isset( $results['errorMessage'] ) ) { ?>
        <div class="errorMessage"><?php echo $results['errorMessage'] ?></div>
<?php } ?>

        <ul>
          <label>SIGN IN</label>
          <li>
            <input type="text" name="username" id="username" placeholder="Your admin username" autofocus maxlength="20" />
          </li>

          <li>
            <input type="password" name="password" id="password" placeholder="Your admin password" maxlength="20" />
          </li>

        </ul>

        <div class="buttons">
          <input type="submit" name="login" value="Login" />
        </div>
        <hr class="line">

        <label>New User?</label>
        <div class="buttons">
          <input type="submit" name="new" value="Sign Up" />
        </div>

      </form>

<?php include "templates/include/footer.php" ?>