<?php include "templates/include/header.php" ?>

      <h1>Profile Archive</h1>

      <ul id="headlines" class="archive">

<?php foreach ( $results['profiles'] as $profile ) { ?>

        <li>
          <p ><?php echo htmlspecialchars( $profile->First_Name )?></p>
        </li>

<?php } ?>

      </ul>

      <p><?php echo $results['totalRows']?> profile<?php echo ( $results['totalRows'] != 1 ) ? 's' : '' ?> in total.</p>

      <p><a href="./">Return to Homepage</a></p>

<?php include "templates/include/footer.php" ?>
