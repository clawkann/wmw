<?php include "templates/include/header.php" ?>

<ul id="headlines">

    <?php foreach ( $results['profiles'] as $profile ) { ?>

    <li>
        <p><?php echo htmlspecialchars( $profile->First_Name )?></p>
    </li>

    <?php } ?>

</ul>

<p><a href="./?action=archive">Profile Archive</a></p>

<?php include "templates/include/footer.php" ?>
