<?php include "templates/include/header.php" ?>


<?php if ( isset( $results['errorMessage'] ) ) { ?>
        <div class="errorMessage"><?php echo $results['errorMessage'] ?></div>
<?php } ?>


<?php if ( isset( $results['statusMessage'] ) ) { ?>
        <div class="statusMessage"><?php echo $results['statusMessage'] ?></div>
<?php } ?>


    <!-- Top --> 
    
    
        
    <div class = "row">    
        <!-- Left Column -->
        <div class="column left white">
            <div>
                <sig><p class = "text-pink", style="text-align:center"><?php echo  $results['profiles'] -> First_Name ?><?php echo " "?><?php echo  $results['profiles'] -> Last_Name ?></p></sig>
                <h1 class = "text-green", style="text-align:center"></><?php echo  $results['profiles'] -> Occupation ?></h1>
            </div>
        </div>  
        
        <!-- Right Column -->          
        <div class="column right">
            <div class="white">
                <div class="display-container pink">
                    <img src="avatar.jpg" style="width:100%" alt="Avatar">
                </div>                
            </div><br>            
        </div>
    </div>
    
    <!-- Bottom -->
    <div class = "row">
        <!-- Left Column -->
        <div class="column left lightbrown">
            <div>
                <h2 class="text-grey">Work Experience</h2>
                <div class="container">
                </div>
            </div>

            <div>
                <h2 class="text-grey ">Education</h2>
                <div class="container">
                </div>
            </div>
        </div>
        
        <!-- Right Column -->
        <div class="column right">
            <div class="grey text-green">
                <div class="container">
                    <p><i class="fa fa-home fa-fw margin-right large"></i> <?php echo  $results['profiles'] -> Location ?></p>
                    <p><i class="fa fa-envelope fa-fw margin-right large"></i><?php echo  $results['profiles'] -> Email ?></p>
                    <p><i class="fa fa-phone fa-fw margin-right large"></i><?php echo  $results['profiles'] -> Phone_Number ?></p>
                    <hr>

                    <p class="large"><b>About Me</b></p>
                    <p> <?php echo  $results['profiles'] -> Blurb ?></p>
                    <hr />

                    <p class="large"><b>Mentorship</b></p>
                    <p>Current mentor: <?php echo  $results['profiles'] -> mentor ?> </p>
                    <p> Current mentee: <?php echo  $results['profiles'] -> mentee ?></p>
                    <p> Looking for mentee: <?php echo  $results['profiles'] -> seeking_mentee ?></p>
                    <p> Looking for mentor:<?php echo  $results['profiles'] -> seeking_mentor ?> </p>
                </div>
            </div><br>

            <!-- End Left Column -->
            
        </div>      
    </div>
  


<?php include "templates/include/footer.php" ?>