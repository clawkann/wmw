-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema wmw
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema wmw
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `wmw` DEFAULT CHARACTER SET latin1 ;
USE `wmw` ;

-- -----------------------------------------------------
-- Table `wmw`.`user`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `wmw`.`user` ;

CREATE TABLE IF NOT EXISTS `wmw`.`user` (
  `id` VARCHAR(50) NOT NULL,
  `password` VARCHAR(50) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `wmw`.`profile`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `wmw`.`profile` ;

CREATE TABLE IF NOT EXISTS `wmw`.`profile` (
  `user_id` VARCHAR(50) NOT NULL,
  `First_Name` VARCHAR(45) NULL DEFAULT NULL,
  `Last_Name` VARCHAR(45) NULL DEFAULT NULL,
  `Birthday` DATE NULL DEFAULT NULL,
  `Occupation` VARCHAR(45) NULL DEFAULT NULL,
  `life_stage` VARCHAR(45) NULL DEFAULT NULL,
  `field` VARCHAR(45) NULL DEFAULT NULL,
  `language` VARCHAR(45) NULL DEFAULT NULL,
  `Email` VARCHAR(45) NULL DEFAULT NULL,
  `Company` VARCHAR(45) NULL DEFAULT NULL,
  `Phone_Number` VARCHAR(45) NULL DEFAULT NULL,
  `Picture` VARCHAR(100) NULL DEFAULT NULL,
  `Blurb` TEXT NULL DEFAULT NULL,
  `seeking_mentor` TINYINT(4) NULL DEFAULT NULL,
  `seeking_mentee` TINYINT(4) NULL DEFAULT NULL,
  `mentor` TINYINT(4) NULL DEFAULT NULL,
  `mentee` TINYINT(4) NULL DEFAULT NULL,
  `Location` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  CONSTRAINT `fk_profile_user`
    FOREIGN KEY (`user_id`)
    REFERENCES `wmw`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;

-- -----------------------------------------------------
-- Table `wmw`.`mentors`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `wmw`.`mentors` (
  `bio_user_id` VARCHAR(50) NOT NULL,
  `bio_user_id1` VARCHAR(50) NOT NULL,
  PRIMARY KEY (`bio_user_id`,`bio_user_id1`),
  CONSTRAINT `fk_mentors_bio`
    FOREIGN KEY (`bio_user_id`)
    REFERENCES `wmw`.`bio` (`user_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_mentors_bio1`
    FOREIGN KEY (`bio_user_id1`)
    REFERENCES `wmw`.`bio` (`user_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;



-- -----------------------------------------------------
-- Table `wmw`.`seeking`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `wmw`.`seeking` ;

CREATE TABLE IF NOT EXISTS `wmw`.`seeking` (
  `user_id` VARCHAR(50) NOT NULL,
  `life_stage` VARCHAR(45) NULL DEFAULT NULL,
  `location` VARCHAR(45) NULL DEFAULT NULL,
  `field` VARCHAR(45) NULL DEFAULT NULL,
  `language` VARCHAR(45) NULL DEFAULT NULL,
  `mentor` TINYINT(4) NOT NULL,
  PRIMARY KEY (`user_id`, `mentor`),
  CONSTRAINT `fk_table1_user10`
    FOREIGN KEY (`user_id`)
    REFERENCES `wmw`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
