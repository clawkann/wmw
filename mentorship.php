<?php

require( "config.php" );
session_start();
$action = isset( $_GET['action'] ) ? $_GET['action'] : "";
$username = isset( $_SESSION['username'] ) ? $_SESSION['username'] : "";

switch ( $action ) {
  case 'editRequest':
    editRequest();
    break;
  case 'deleteRequest':
    deleteRequest();
    break;
  default:
    mentor_homepage();
}

function editRequest() {
  $results = array();
  $results['pageTitle'] = "Edit Request";
  $results['formAction'] = "editRequest";
  
  if (isset($_POST['saveChanges'])){
   if ( !Requests::getUser( $_SESSION['username'], $_POST['mentor']) ) {
        $request = new Requests;
        $request -> user_id = $_SESSION['username'];
        $request->storeFormValues( $_POST );
        $request->insert();  
    }else{
        $data = Requests::getByUser( $_SESSION['username'], $_POST['mentor']); 
        $request = $data['results'][0];
        $request->storeFormValues( $_POST );
        $request->update();
    
    }
    header( "Location: mentorship.php?action=changesSaved" );
  
  } elseif ( isset($_POST['cancel'])){
      header( "Location: mentorship.php?action=cancel" );  
  }else{
      if (array_key_exists('mentor', $_GET)) {
            $data = Requests::getByUser( $_SESSION['username'],  $_GET['mentor'] ); 
            $results['request'] = $data['results'][0]; 
            $ment = $results['request'] -> mentor; 
    }else{
          $results['request'] = new Requests;
          if (Requests::getUser($_SESSION['username'], 1)){
               $ment = 0;
          } elseif (Requests::getUser($_SESSION['username'], 0)){
              $ment = 1;
          } else {
              $ment = 2;
          }
          }
             require(TEMPLATE_PATH . "/mentor/editRequest.php");
      }
   
}




function mentor_homepage() {
  $results = array();
  $data_mentor = Profile::getListMentors($_SESSION['username']);
  $results['mentors'] = $data_mentor['results'];
  $data_mentees =  Profile::getListMentees($_SESSION['username']);
  $results['mentees'] = $data_mentees['results'];
  $data_requests = Requests::getByUser($_SESSION['username'], 2);
  $results['requests'] = $data_requests['results'];
  require( TEMPLATE_PATH . "/mentor/mentor.php" );
  
}

function deleteRequest(){
    $data = Requests::getByUser($_SESSION['username'], $_GET['mentor']);
    $data['results'][0] -> delete();
    header( "Location: mentorship.php");
}

?>
