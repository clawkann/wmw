<?php

require( "config.php" );
session_start();
$action = isset( $_GET['action'] ) ? $_GET['action'] : "";
$username = isset( $_SESSION['username'] ) ? $_SESSION['username'] : "";

if ( $action != "login" && $action != "logout" && !$username ) {
  login();
  exit;
}

switch ( $action ) {
  case 'login':
    login();
    break;
  case 'logout':
    logout();
    break;
  case 'newProfile':
    newProfile();
    break;
  case 'editProfile':
    editProfile();
    break;
  case 'deleteProfile':
    deleteProfile();
    break;
  default:
    listProfiles();
}


function login() {

  $results = array();
  $results['pageTitle'] = "Admin Login | Widget News";
    if ( isset( $_POST['new'] ) ) {
    require(TEMPLATE_PATH. "/admin/newUser.php");
    }
    

  if ( isset( $_POST['login'] ) ) {

    // User has posted the login form: attempt to log the user in

    if ( $_POST['username'] == ADMIN_USERNAME && $_POST['password'] == ADMIN_PASSWORD ) {

      // Login successful: Create a session and redirect to the admin homepage
      $_SESSION['username'] = ADMIN_USERNAME;
      header( "Location: ../admin.php" );

    } else {

      // Login failed: display an error message to the user
      $results['errorMessage'] = "Incorrect username or password. Please try again.";
      require( TEMPLATE_PATH . "/admin/loginForm.php" );
    }

  } else {

    // User has not posted the login form yet: display the form
    require( TEMPLATE_PATH . "/admin/loginForm.php" );
  }

}


function logout() {
  unset( $_SESSION['username'] );
  header( "Location: admin.php" );
}


function newProfile() {

  $results = array();
  $results['pageTitle'] = "New Profile";
  $results['formAction'] = "newProfile";

  if ( isset( $_POST['saveChanges'] ) ) {

    // User has posted the article edit form: save the new article
    $profile = new Profile;
    $profile->storeFormValues( $_POST );
    $profile->insert();
    header( "Location: admin.php?status=changesSaved" );

  } elseif ( isset( $_POST['cancel'] ) ) {

    // User has cancelled their edits: return to the article list
    header( "Location: admin.php" );
  } else {

    $results['profile'] = new Profile;
    require( TEMPLATE_PATH . "/admin/editProfile.php" );
  }

}


function editProfile() {

  $results = array();
  $results['pageTitle'] = "Edit Profile";
  $results['formAction'] = "editProfile";

  if ( isset( $_POST['saveChanges'] ) ) {

    // User has posted the article edit form: save the article changes

    if ( !$profile = Profile::getByUser( $_POST['userId'] ) ) {
      header( "Location: admin.php?error=articleNotFound" );
      return;
    }

    $article->storeFormValues( $_POST );
    $article->update();
    header( "Location: admin.php?status=changesSaved" );

  } elseif ( isset( $_POST['cancel'] ) ) {

    // User has cancelled their edits: return to the article list
    header( "Location: admin.php" );
  } else {

    // User has not posted the article edit form yet: display the form
    $results['profile'] = Profile::getByUser( $_GET['userId'] );
    require( TEMPLATE_PATH . "/admin/editProfile.php" );
  }

}


function deleteArticle() {

  if ( !$profile = Profile::getByUser( $_GET['userId'] ) ) {
    header( "Location: admin.php?error=articleNotFound" );
    return;
  }

  $article->delete();
  header( "Location: admin.php?status=articleDeleted" );
}


function listProfiles() {
  $results = array();
  $data = Profile::getList();
  $results['profiles'] = $data['results'];
  $results['totalRows'] = $data['totalRows'];
  $results['pageTitle'] = "All Profiles";

  if ( isset( $_GET['error'] ) ) {
    if ( $_GET['error'] == "profileNotFound" ) $results['errorMessage'] = "Error: Profile not found.";
  }

  if ( isset( $_GET['status'] ) ) {
    if ( $_GET['status'] == "changesSaved" ) $results['statusMessage'] = "Your changes have been saved.";
    if ( $_GET['status'] == "articleDeleted" ) $results['statusMessage'] = "Article deleted.";
  }

  require( TEMPLATE_PATH . "/admin/listProfiles.php" );
}
