<?php

require( "config.php" );
session_start();
$action = isset( $_GET['action'] ) ? $_GET['action'] : "";
$username = isset( $_SESSION['username'] ) ? $_SESSION['username'] : "";

if ( $action != "logout"  && $action != "editProfile" && !$username && $action != 'newUser' && $action != 'newProfile') {
       login(); 
  exit;
}

switch ( $action ) {
  case 'login':
    login();
    break;
  case 'logout':
    logout();
    break;
  case 'newUser':
    login_new();
    break;
  case 'editProfile':
    editProfile();
    break; 
  case 'listProfiles':
    listProfiles();
    break; 
  default:
    viewProfiles();
}


function login() {

  $results = array();
  $results['pageTitle'] = "Login";
   
  if ( isset( $_POST['login'] ) ) {

    // User has posted the login form: attempt to log the user in
    $out = User::getByUser($_POST['username'], $_POST['password']);
    print_r($_POST);
    if ( $out == "1") {

      // Login successful: Create a session and redirect to the admin homepage
      $_SESSION['username'] =  $_POST['username'];
      header( "Location: index.php?action=successLogin" );

    } elseif ( isset( $_POST['new'] ) ) {
        header( "Location: index.php?action=newUser" );
    }else {
      //echo $_POST['username'];
      // Login failed: display an error message to the user
      $results['errorMessage'] = "Incorrect username or password. Please try again.";
      require( TEMPLATE_PATH . "/loginForm.php" );
    }

  } else {

    // User has not posted the login form yet: display the form
    require( TEMPLATE_PATH . "/loginForm.php" );
  }
  


}


function logout() {
  unset( $_SESSION['username'] );
  header( "Location: index.php" );
}


function login_new() {

  $results = array();
  $results['pageTitle'] = "New User";
  $results['formAction'] = "newUser";


  if ( isset( $_POST['saveChanges'] ) ) {
    $user = new User;
    $user -> storeFormValues($_POST);
    $user -> insert();
    // User has posted the login form: attempt to log the user in
    header( "Location: index.php?status=editProfile" );
    

  } elseif ( isset( $_POST['cancel'] )) {
       echo 'Cancel';
  
     header( "Location: index.php" );}
     else{


     $results['user'] = new User;
    // User has not posted the login form yet: display the form
    require( TEMPLATE_PATH . "/newUser.php" );
  }
  


}


function editProfile() {

  $results = array();
  $results['pageTitle'] = "Edit Profile";
  $results['formAction'] = "editProfile";

  if ( isset( $_POST['saveChanges'] ) ) {

    if ( !Profile::getUser( $_SESSION['username']) ) {
        $profile = new Profile;
        $profile -> user_id = $_SESSION['username']; 
        $profile->storeFormValues( $_POST );

        $profile->insert();
      
    }else{
    
        $profile = Profile::getByUser( $_SESSION['username']); 
        $profile->storeFormValues( $_POST );
        $profile->update();
    }
   header( "Location: index.php?status=changesSaved" );

  } elseif ( isset( $_POST['cancel'] ) ) {
    header( "Location: index.php" );
  } else {
    if (Profile::getUser( $_SESSION['username'])){

        $results['profile'] = Profile::getByUser(  $_SESSION['username'] );
    }else{
        $results['profile'] = new Profile; }
    require( TEMPLATE_PATH . "/editProfile.php" );
  }

}


function deleteProfile() {

  if (Profile::getUser($_SESSION['username'] ) ) {
   $profile = Profile::getByUser($_SESSION['username']);
   $profile -> delete();
    header( "Location: index.php?status=profileDeleted" );
    return;
  } else {
      header( "Location: index.php?status=notFound" );
  }

}


function viewProfiles() {
      $results = array();
      if ( !isset($_GET["userId"]) || !$_GET["userId"]){
          if(Profile::getUser(  $_SESSION['username'])){
              $results['profiles'] = Profile::getByUser(  $_SESSION['username']);
              $results['pageTitle'] =  $_SESSION['username'];
              require( TEMPLATE_PATH . "/viewProfile.php" );
          } else{
          header( "Location: index.php?action=editProfile" );
   }} else{
          if(Profile::getUser(  $_GET["userId"])){
              $results['profiles'] = Profile::getByUser( $_GET["userId"]);
              $results['pageTitle'] = $_GET["userId"];
              require( TEMPLATE_PATH . "/viewProfile.php" );
         }else{
              if(Profile::getUser(  $_SESSION['username'])){
                  $results['profiles'] = Profile::getByUser(  $_SESSION['username']);
                  $results['pageTitle'] =  $_SESSION['username'];
                  require( TEMPLATE_PATH . "/viewProfile.php" );
              } else{
                  header( "Location: index.php?action=editProfile" );
              }
          }
      
      }
}

function listProfiles() {
  $results = array();
  $data = Profile::getList();
  $results['profiles'] = $data['results'];
  $results['totalRows'] = $data['totalRows'];
  $results['pageTitle'] = "All Articles";
  
  if ( isset( $_GET['error'] ) ) {
  if ( $_GET['error'] == "profileNotFound" ) $results['errorMessage'] = "Error: Profile not found.";
}
  require( TEMPLATE_PATH . "/listProfiles.php" );
}

