<?php
//THIS FILE SHOULD BE STORED LOCALLY AND NOT CHANGE
ini_set( "display_errors", true );
date_default_timezone_set( "America/Los_Angeles" );
define( "DB_DSN", "mysql:host=35.236.43.190;dbname=wmw" );
define( "DB_USERNAME", "root" );
define( "DB_PASSWORD", "wmw" );
define( "CLASS_PATH", "classes" );
define( "TEMPLATE_PATH", "templates" );
define( "ADMIN_USERNAME", "admin" );
define( "ADMIN_PASSWORD", "mypass" );
require( CLASS_PATH . "/Profile.php" );
require( CLASS_PATH . "/User.php" );
require( CLASS_PATH . "/Requests.php" );
require( CLASS_PATH . "/Bio.php" );

function handleException( $exception ) {
  echo "Sorry, a problem occurred. Please try later.";
  echo ( $exception->getMessage() );
}

set_exception_handler( 'handleException' );
?>
